__author__ = 'Klosjan'

import unittest
from selenium import webdriver


class PageSearch(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(10)
        self.driver.get("http://sii.pl/en/branches/gdansk/")

    def test_email_telephone(self):
        driver = self.driver
        self.assertIn("58 32 17 800",
                      driver.page_source,
                      "Telephone number not found")
        # mozna tez po regexp
        # self.assertRegexpMatches(self.driver.find_element_by_xpath("html/body").text,
                                 # r"^[\s\S]*58 32 17 800[\s\S]*$")
        self.assertIn("mailto:informacja-gdansk@pl.sii.eu",
                      driver.page_source,
                      "Email not found")

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()