# -*- coding: utf-8 -*-
__author__ = 'Klosjan'

import unittest
from selenium import webdriver


class GoogleResultCheck(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(10)
        # mozna od razu po gotowym url z query
        # driver.get("https://www.google.com/?gws_rd=cr&q=site:sii.pl+www")
        # wyszukiwanie w google.com, bo w lokalnym google.pl wynik ma inny tytul: "Gdansk - Sii"
        self.driver.get("https://www.google.com/?gws_rd=cr")
        search_input = self.driver.find_element_by_id("lst-ib")
        search_input.clear()
        search_input.send_keys("site:sii.pl www")
        submit_button = self.driver.find_element_by_name("btnG")
        submit_button.click()

    def test_4th_result(self):
        driver = self.driver
        link_text = driver.find_element_by_xpath("//*[@id='rso']/div[2]/div[4]/div/h3/a",).text
        self.assertEqual("Gdansk - Sii Polska",
                         link_text,
                         "Incorrect title: " + link_text)
        url_text = driver.find_element_by_xpath("//*[@id='rso']/div[2]/div[4]/div/div/div/div/cite").text
        self.assertEqual("sii.pl/en/branches/gdansk/",
                         url_text,
                         "Incorrect url: " + url_text)
        description = driver.find_element_by_xpath(u"//*[@id='rso']/div[2]/div[4]/div/div/div/span").text
        # uzywam regexp, zeby pass, bo przy assertEqual brakuje "virtual tour ..." na koncu textu
        self.assertRegexpMatches(description,
                                 ur"^[\s\S]*Our Officebest place to work in IT. fot. Michał Ozdoba - "
                                 ur"Photographer http://www.facebook.com/. 200m to the Railway Station.[\s\S]*$")
        # "Tłumaczenie strony" nie zawsze sie pojawia przy kazdym wyniku, mozna zakomentowac, zeby pass
        self.assertEqual(u"Tłumaczenie srony",
                         driver.find_element_by_xpath(u"//*[@id='rso']/div[2]/div[4]/div/div/div/div/a").text)

    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()